<?php

use Illuminate\Support\Facades\Route;


Auth::routes();


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'auth'], function(){
    Route::get('/', 'DashboardController@index')->name('welcome');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // route setup learning paths
    Route::resource('lpaths', LearningPathController::class);
    Route::resource('courses', CoursesController::class);
});