@extends('layouts.admin.app')
@section('pages', 'Created Learning Paths')
@push('links')

@endpush
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Add Learning Paths</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="{{route('admin.lpaths.store')}}" enctype="multipart/form-data"> 
            @csrf
            <div class="form-group">
                <label for="title">Paths Name</label>
                <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" autofocus>

                @error('title')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror

            </div>
            <div class="form-group">
                <label for="description">Paths Description</label>
                <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="4"></textarea>
            
                @error('description')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror

            </div>
            <div class="form-group">
                <!-- <label for="customFile">Custom File</label> -->

                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="cover" onchange="previewFile(this)" name="cover">
                    <label class="custom-file-label" for="cover">Choose file</label>
                    <img id="viewCover" src="" alt="" style="max-width: 130px;margin-top: 20px;max-height: 130px;" />
                </div>

                @error('cover')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror
            </div>
            {{-- <div class="form-group">
                <label for="inputStatus">Status</label>
                <select class="form-control custom-select">
                    <option selected="" disabled="">Select one</option>
                    <option>On Hold</option>
                    <option>Canceled</option>
                    <option>Success</option>
                </select>
            </div> --}}
            
            <!-- /.card-body -->
            <div class="row">
                <div class="col-md-6 my-2">
                    <a href="#" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Add new" class="btn btn-info">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')

{{-- use previewFile Photo Upload function  --}}
<script type="text/javascript">

document.querySelector('.custom-file-input').addEventListener('change',function(e){
    var fileName = document.getElementById("cover").files[0].name;
    var nextSibling = e.target.nextElementSibling
    nextSibling.innerText = fileName
});

</script>
@endpush