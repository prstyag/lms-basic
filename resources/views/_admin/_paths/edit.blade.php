@extends('layouts.admin.app')
@section('pages', 'Edited Learning Paths')
@push('links')

@endpush
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Edit Learning Paths</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="{{route('admin.lpaths.update', $paths->slug)}}" enctype="multipart/form-data"> 
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Paths Name</label>
                <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" autofocus value="{{$paths->title}}">

                @error('title')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror

            </div>
            <div class="form-group">
                <label for="description">Paths Description</label>
                <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="4">{{$paths->description}}</textarea>
            
                @error('description')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror

            </div>


            <div class="form-group">
                <!-- <label for="customFile">Custom File</label> -->

                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="cover" onchange="previewFile(this)" name="cover">
                    <label class="custom-file-label" for="cover">Choose file...</label>
                    <img id="viewCover" src="{{asset('storage/paths_cover/')}}/{{$paths->cover}}" alt="" style="max-width: 130px;margin-top: 20px;max-height: 130px;" /> <br>
                </div>

                @error('cover')
                    <div class="alert" style="color: red;font-size:13px;"><i>{{ $message }}</i></div>
                @enderror
            </div>
            <!-- /.card-body -->
            <div class="row">
                <div class="col-md-6 my-5">
                    <a href="#" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Edit now" class="btn btn-info">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
{{-- use previewFile Photo Upload function  --}}
<script type="text/javascript">

function previewFile(input){
    var file=$("input[type=file]").get(0).files[0];

    if(file)
    {
        var reader = new FileReader();
        reader.onload = function(){
            $('#previewImg').attr("src", reader.result);
        }
        reader.readAsDataURL(file);
    }
};
    
</script>
@endpush