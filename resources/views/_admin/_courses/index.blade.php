@extends('layouts.admin.app')
@section('pages', 'Courses')
@push('links')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap.min.css">
@endpush
@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Listing Courses {{$courses->count()}}</h3>
    <a href="{{route('admin.courses.create')}}" class="card-title float-right"><i class="fas fa-plus" style="color: green"></i></a>
    </div>
    <div class="card-body">
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Created</th>
                        <th>Name</th>
                        <th>Cover</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (($courses->count()) > 0)
                    @foreach ($courses as $course)
                            <tr>
                                <td>{{ $course->created_at->format('D d/m/Y') }}</td>
                                <td>{{ $course->title }}</td>
                                <td class="text-center py-0 align-middle"><img src="{{ asset('storage/course/'.$course->cover) }}" alt="{{ $course->title }}" style="max-width: 50px;margin-top: 2px;"></td>
                                <td>{{ Str::limit($course->description, 50) }}</td>
                                <td class="text-center py-0 align-middle">
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-info" data-toggle="modal" id="mediumButton" data-target="#modalView{{ $course->id }}"><i class="fas fa-eye" style="color: azure"></i></a>

                                        <a href="{{route('admin.lpaths.edit', $course->slug)}}" class="btn btn-success"><i class="fas fa-edit"></i></a>

                                        <a class="btn btn-danger" data-toggle="modal" id="mediumButton" data-target="#modalDelete{{ $course->slug }}"><i class="fas fa-trash" style="color: azure"></i></a>
                                    </div>
                                </td>
                            </tr>
                    @endforeach
                @else
            
                    <td colspan="6" class="text-center" style="color: #6777ef;">                           
                        <b>Course not found. <a href="{{route('admin.lpaths.create')}}" style="font-weight: bold;">Create here!</a></b>
                    </td>
                    
                @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>Created</th>
                        <th>Name</th>
                        <th>Cover</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!-- Modal view-->
@foreach ($courses as $course)    
<div class="modal fade" id="modalView{{ $course->id }}" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">{{$course->title}}</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>{{$course->description}}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="{{ route('admin.lpaths.edit', $course->slug) }}" class="btn btn-info">Edit</a>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach

<!-- Modal delete-->
@foreach ($courses as $course)    
<div class="modal fade" id="modalDelete{{ $course->slug }}" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">You are sure delete this paths?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p><i>{{$course->title}}</i></p>
        </div>
        <div class="modal-footer">
            <form action="{{route('admin.lpaths.destroy', $course->slug) }}" method="POST">
                @csrf
                @method('delete')
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-action">Yes, Delete</button>
            </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach

{{-- attribute tag a --}}
{{-- data-toggle="modal" id="mediumButton" data-target="#mediumModal"
data-attr="{{ route('projects.edit', $project->id) }}" --}}

<!-- Pager -->
{{-- <div class="clearfix mt-4">
    {{$posts->links('vendor.pagination.bootstrap-4')}}
</div> --}}

@endsection
@push('scripts')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap.min.js"></script>
<script>
    $(function () {
        $("#example").DataTable({
        "responsive": true,
        "autoWidth": false,
        });
    });

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
</script>
@endpush