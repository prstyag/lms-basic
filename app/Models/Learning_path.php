<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Learning_path extends Model
{
    use HasFactory;

    protected $table = 'learning_paths';
    public $timestamps = true;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function courses(){
        return $this->belongsTo(Courses::class);
    }
}
