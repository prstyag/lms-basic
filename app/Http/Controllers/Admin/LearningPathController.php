<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Learning_path;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class LearningPathController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paths = Learning_path::orderBy('created_at','desc')->get();
        return view('_admin._paths.index', compact('paths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('_admin._paths.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:learning_paths|min:3|max:255',
            'description' => 'required',
            'cover' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails()){
            Alert::toast('New paths unsuccessfully added!', 'error');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        };

        // setup cover image
        $cover = $request->file('cover');
        $cover_name = time().'.'.$cover->extension();
        $request->cover->storeAs('paths_cover', $cover_name, 'public');

        $paths = new Learning_path();
        $paths->title = request()->input('title');
        $paths->description = request()->input('description');
        $paths->slug = Str::slug($request->input('title', '-'));
        $paths->cover = $cover_name;
        $paths->save();

        Alert::toast('New paths successfully added!', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $paths = Learning_path::where('slug', $slug)->first();
        return view('_admin._paths.edit', compact('paths'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:learning_paths|min:3|max:255',
            'description' => 'required',
        ]);

        if ($validator->fails()){
            Alert::toast('Paths unsuccessfully edited!', 'error');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        };

        $paths = Learning_path::where('slug', $slug)->first();
        if ($request->hasFile('cover')) {

            $cover = $request->file('cover');
            $cover_name = time().'.'.$cover->extension();
            $request->cover->storeAs('paths_cover', $cover_name, 'public');


            $paths->title = request()->input('title');
            $paths->description = request()->input('description');
            $paths->slug = Str::slug($request->input('title', '-'));
            $paths->cover =$cover_name;
        } else {
            $paths->title = request()->input('title');
            $paths->description = request()->input('description');
            $paths->slug = Str::slug($request->input('title', '-'));
        }
        
        $paths->update();

        Alert::toast('Paths successfully edited!', 'success');
        return redirect('admin/lpaths');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $paths = Learning_path::where('slug', $slug)->first();
        $paths->delete();

        Alert::toast('Paths successfully deleted!', 'success');
        return redirect()->back();
    }
}
