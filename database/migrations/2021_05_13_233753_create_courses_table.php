<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('learning_path_id');
            $table->string('title', 255);
            $table->longText('description')->nullable();
            $table->longText('disclamer')->nullable();
            $table->string('thumbnails')->default('image.jpg');
            $table->double('discount_price',10,2);
            $table->double('actual_price',10,2);
            $table->string('slug');

            // foreign 
            $table->foreign('learning_path_id')->references('id')->on('learning_paths')
            ->onUpdate('cascade')->onDelete('cascade');   

            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
